package main

import (
    "fmt"
    "time"
)

func fork(chfork chan bool) {
    for {
        chfork<- true
        <-chfork
    }
}
func philosopher(rightFork, leftFork chan bool) {
    for {
        fmt.Println("Thinking")
        <-rightFork
        <-leftFork
        fmt.Println("Eating")
        rightFork<- true
        leftFork<- true
    }
}
func main() {
    n := 5
    forks := make([]chan bool, n)
    for i := range forks {
        forks[i] = make(chan bool, 1)
    }
    for i := range forks {
        go fork(forks[i])
        go philosopher(forks[i], forks[(i+1)%n])
    }
    time.Sleep(time.Second*10)
}
