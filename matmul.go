package main

import "fmt"

func multiplier(firstElement float64, n,e,s,w chan float64) {
	for secondElement := range n {
		sum := <-e
		sum = sum + firstElement*secondElement
		s<- secondElement
		w<- sum
	}
	close(s)
	close(w)
}
func source(row []float64, s chan float64) {
	for _, v := range row {
		s<- v
	}
	close(s)
}
func zero(n int, w chan float64) {
	for i := 0; i < n; i++ {
		w<- .0
	}
	close(w)
}
func sink(n chan float64) {
	for _ = range n {}
}
func result(c [][]float64, i int, e chan float64) {
	j := 0
	for v := range e {
		c[i][j] = v
		j++
	}
	fin<- true
}
var fin chan bool
func main() {
	a := [][]float64{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}
	b := [][]float64{{1, 0, 2}, {0, 1, 2}, {1, 0, 0}}
	c := [][]float64{{0, 0, 0}, {0, 0, 0}, {0, 0, 0}}
	nra := len(a)
	nca := len(a[0])
	nrb := len(b)
	fin = make(chan bool)
	h := make([][]chan float64, nra)
	for i := range h {
		h[i] = make([]chan float64, nca+1)
		for j := range h[i] {
			h[i][j] = make(chan float64)
		}
	}
	v := make([][]chan float64, nca+1)
	for i := range v {
		v[i] = make([]chan float64, nrb)
		for j := range v[i] {
			v[i][j] = make(chan float64)
		}
	}
	for i := range a {
		go zero(nrb, h[i][nca])
		go result(c, i, h[i][0])	
	}
	for i := range b {
		go source(b[i], v[0][i])
		go sink(v[nra][i])
	}
	for i, row := range a {
		for j, f := range row {
			go multiplier(f, v[i][j], h[i][j+1],
				             v[i+1][j], h[i][j])
		}
	}
	for _ = range a {
		<-fin
	}
	fmt.Println(c)
}