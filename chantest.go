package main

import (
	"fmt"
	"time"
)

func main() {
	ch := make(chan bool)

	go (func() {
		fmt.Println("P2")
		<-ch
		fmt.Println("P22")
	})()
	go (func() {
		fmt.Println("P1")
		ch<- true
		fmt.Println("P11")
	})()

	time.Sleep(time.Second)
	<-ch
	fmt.Println(":)")
}