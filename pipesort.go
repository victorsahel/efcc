package main

import (
    "fmt"
    "math"
    "math/rand"
)

func sort(in, out chan float64, i int, s[]float64) {
    p := -1
    min := math.Inf(0)
    temp := make([]float64, 0, 20)
    for v := range in {
        if v < min {
            min = v
            p = len(temp)
        }
        temp = append(temp, v)
    }
    for i, v := range temp {
        if i != p {
            out<- v
        }
    }
    close(out)
    s[i] = min
}
func main() {
    n := 20
    ch := make([]chan float64, n+1)
    ch[0] = make(chan float64)
    go func(ch chan float64) {
        rand.Seed(1981)
        for i := 0; i < n; i++ {
            ch<- float64(rand.Intn(100))
        }
        close(ch)
    }(ch[0])
    s := make([]float64, n)
    for i := 0; i < n; i++ {
        ch[i+1] = make(chan float64)
        go sort(ch[i], ch[i+1], i, s)
    }
    for _ = range ch[n] {}
    fmt.Println(s)
}
