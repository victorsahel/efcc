package main

import (
	"bufio"
    "fmt"
    "net"
	"encoding/json"
	"strconv"
	"strings"
)

var ids map[int]bool = make(map[int]bool)// cadena de booleanos con clave entera

func Mensaje(cn net.Conn)string{
	r:=bufio.NewReader(cn)
	msg, _ := r.ReadString('\n')
	return strings.TrimSpace(msg)
}
func EnviarNo(toIp int, msg string){
	cn1, _ := net.Dial("tcp", "localhost:"+strconv.Itoa(toIp))
	defer cn1.Close()
	fmt.Fprintln(cn1,msg)
}
func Enviar(toIp int, msg string)string{
	con2, _ := net.Dial("tcp", "localhost:"+strconv.Itoa(toIp))
	defer con2.Close()
	fmt.Fprintln(con2, msg)
	return Mensaje(con2)
}
func dirIP(chId <- chan int){
	for{
		ids[<-chId] = true
		fmt.Println(ids)
	}
}
func AgregarServidor(id int, chId chan<-int){
	new:=id+1
	ln, _ := net.Listen("tcp","localhost:"+strconv.Itoa(new))
	defer ln.Close()
	for {
			cn, _ :=ln.Accept()
			go func(cn net.Conn){
				val, _ := strconv.Atoi(Mensaje(cn))
				chId<- val
				cn.Close()
			}(cn)
	}
}
func AgregarCliente(newId int, ids map[int]bool){
	msg:= fmt.Sprintf("%d", newId)
	for target := range ids{
		go EnviarNo(target+1,msg)
	}
}
func RegistrarServidor(id int, chId, end chan<-int){
	var new=id+2
	ln, _ := net.Listen("tcp", "localhost:"+strconv.Itoa(new))
	defer ln.Close()
	for{
		cn, _ := ln.Accept()
		go func(cn net.Conn){
			newId, _ := strconv.Atoi(Mensaje(cn))
			AgregarCliente(newId,ids)
			buf, _ := json.Marshal(ids)
			fmt.Println(cn, string(buf))
			chId<-newId
			defer cn.Close()
		}(cn)
	}
	end<-0
}
func RegistrarCliente(id, targetId int, chId chan<-int){
	resp:=Enviar(targetId+2, fmt.Sprintf("%d",id))
	var slc map[int]bool
	_=json.Unmarshal([]byte(resp), &slc)
	for newId := range slc {
		chId<-newId
	}
}
func main(){
	var corrida=1;
	for {
			fmt.Print(corrida)
			corrida=corrida+1
			chId:=make(chan int)
			end:= make(chan int)
			go dirIP(chId)
			id:=0
			fmt.Print("Ingresa tu Puerto:")
			fmt.Scanf("%d\n", &id)
			go AgregarServidor(id,chId)
			go RegistrarServidor(id,chId,end)
			friend:=0
			fmt.Print("Ingresa el friend port")
			fmt.Scanf("%d\n",&friend)
			if id != friend{
				chId <- friend
				RegistrarCliente(id, friend,chId)
			}
			<-end
	}
}

