package main

import (
    "fmt"
    "math/rand"
)

type Node struct {
    Left    *Node
    Value   int
    Right   *Node
}

type Tree struct {
    root    *Node
}

func add(n *Node, e int) *Node {
    if n == nil {
        return &Node{Value: e}
    } else if e > n.Value {
        n.Right = add(n.Right, e)
    } else {
        n.Left = add(n.Left, e)
    }
    return n
}
func inOrder(n *Node, f func(int)) {
    if n != nil {
        inOrder(n.Left, f)
        f(n.Value)
        inOrder(n.Right, f)
    }
}

func (t *Tree)Add(e int) {
    t.root = add(t.root, e)
}
func (t *Tree)InOrder(f func(int)) {
    inOrder(t.root, f)
}

func rndTree(s []int) *Tree {
    for i := len(s)-1; i > 0; i-- {
        p := rand.Intn(i)
        s[i], s[p] = s[p], s[i]
    }
    t := new(Tree)
    for _, v := range s {
        t.Add(v)
    }
    return t
}

func check(t1, t2 *Tree) bool {
    ch1 := make(chan int)
    ch2 := make(chan int)
    go t1.InOrder(func(a int) {ch1<-a})
    go t2.InOrder(func(a int) {ch2<-a})
    for i := 0; i < 6; i++ {
        if <-ch1 != <-ch2 {
            return false
        }
    }
    return true
}

func main() {
    prnt := func(a int) {
        fmt.Println(a)
    }
    d1 := []int{6, 5, 4, 3, 2, 1}
    t1 := rndTree(d1)
    t1.InOrder(prnt)
    d2 := []int{6, 8, 4, 3, 2, 1}
    t2 := rndTree(d2)
    t2.InOrder(prnt)
    if check(t1, t2) {
        fmt.Println(":)")
    }
}
